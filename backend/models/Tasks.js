const mongoose = require('mongoose');
const Joi = require('joi');

const TaskJoiSchema = Joi.object({
  name: Joi.string().min(3)
    .max(30)
    .required(),

  description: Joi.string().min(3)
    .max(400)
    .required(),

  status: Joi.string()
    .valid('ToDo', 'InProgress', 'Done', 'Archived')
    .required()

});

const TaskSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },

  description: {
    type: String,
    required: true,
  },

  created_by: {
    type: String,
    required: false,
  },
  board: {
    type: String,
    required: true,
  },

  status: {
    type: String,
    required: true,
  },

  createdDate: {
    type: String,
    required: false,
  },
});

TaskSchema.methods.toJSON = function () {
  const obj = this.toObject();

  delete obj.__v;

  return obj;
};

const Task = mongoose.model('Task', TaskSchema);
const Archive = mongoose.model('Archive', TaskSchema);

module.exports = {
  TaskJoiSchema,
  Task,
  Archive
};
