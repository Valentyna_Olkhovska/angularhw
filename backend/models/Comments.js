const mongoose = require('mongoose');
const Joi = require('joi');

const CommentJoiSchema = Joi.object({
  name: Joi.string().min(3)
    .max(30)
    .required(),

  text: Joi.string().min(3)
    .max(400)
    .required(),

});

const CommentSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },

  text: {
    type: String,
    required: true,
  },
  task: {
    type: String,
    required: true,
  },

  createdDate: {
    type: String,
    required: false,
  },
});

CommentSchema.methods.toJSON = function () {
  const obj = this.toObject();

  delete obj.__v;

  return obj;
};

const Comment = mongoose.model('Comment', CommentSchema);


module.exports = {
    CommentJoiSchema,
    Comment,
};
