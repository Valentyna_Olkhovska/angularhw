const mongoose = require('mongoose');
const Joi = require('joi');
const { number } = require('joi');

const BoardJoiSchema = Joi.object({
    name: Joi.string().min(3)
        .max(30)
        .required(),

    description: Joi.string().min(3)
        .max(400)
        .required(),
});

const EditBoardSchema = BoardJoiSchema.fork(['description'], (schema) => schema.optional())

const BoardSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },

    description: {
        type: String,
        required: true,
    },

    created_by: {
        type: String,
        required: false,
    },
    tasks: [
        {
            name: {
                type: String,
            },
            taskId: {
                type: String,
            },
            status: {
                type: String,
            },
        }
    ],

    createdDate: {
        type: String,
        required: false,
    },


});

BoardSchema.methods.toJSON = function () {
    const obj = this.toObject();

    delete obj.__v;

    return obj;
};

const Board = mongoose.model('Board', BoardSchema);

module.exports = {
    BoardJoiSchema,
    EditBoardSchema,
    Board,
};
