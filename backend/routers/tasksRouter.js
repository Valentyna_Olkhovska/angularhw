const express = require('express');

const router = new express.Router();
const asyncHandler = require('express-async-handler');

const {
  addTask,
  getTaskById,
  updateTask,
  updateTaskStatus,
  deleteTask,

} = require('../services/tasksService');

// const { authMiddleware } = require('../middleware/authMiddleware');

router.post('/:id/task/create', asyncHandler(addTask));

router.get('/task/:taskId', asyncHandler(getTaskById));

router.put('/task/:taskId', asyncHandler(updateTask));

router.patch('/task/:taskId', asyncHandler(updateTaskStatus));

router.delete('/task/:taskId', asyncHandler(deleteTask));


module.exports = {
  tasksRouter: router,
};
