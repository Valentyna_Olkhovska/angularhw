const express = require('express');

const router = new express.Router();
const asyncHandler = require('express-async-handler');

const {
getCommentTask,
getAllComments,
addComment

} = require('../services/commentService');

// const { authMiddleware } = require('../middleware/authMiddleware');

router.post('/:id', asyncHandler(addComment));

router.get('/:id/comments', asyncHandler(getAllComments));

router.get('/:id', asyncHandler(getCommentTask));



module.exports = {
  commentRouter: router,
};
