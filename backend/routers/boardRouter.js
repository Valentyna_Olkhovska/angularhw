const express = require('express');

const router = new express.Router();
const asyncHandler = require('express-async-handler');

const {
  getBoards,
  getBoardTasks,
  getBoardById,
  addBoard,
  updateBoard,
  deleteBoard,

} = require('../services/boardService');

router.get('/',asyncHandler(getBoards));

router.get('/tasks/:id',asyncHandler(getBoardTasks));

router.get('/:id',asyncHandler(getBoardById));

router.post('/',asyncHandler(addBoard));

router.patch('/:id',asyncHandler(updateBoard));

router.delete('/:id',asyncHandler(deleteBoard));


module.exports = {
  boardRouter: router,
};
