// const bcrypt = require('bcryptjs');
// const jwt = require('jsonwebtoken');
// const { Types } = require('mongoose');
// const fs = require('fs');
// const path = require('path');

// const { User, UserJoiSchema } = require('../models/Users.js');

// const MongoErrors = {
//   DUPLICATE_CODE: 11000,
// };

// const registerUser = async (req, res) => {
//   const { password,email } = req.body;
//   await UserJoiSchema.validateAsync({ password,email });

//   const user = new User({
//     password: await bcrypt.hash(password, 10),
//     email,
//     createdDate: new Types.ObjectId().getTimestamp().toISOString(),
//   });

//   try {
//     await user.save();
//   } catch (error) {
//     if (error.code === MongoErrors.DUPLICATE_CODE) {
//       res
//         .status(400)
//         .send({ message: 'User already exist!' });

//       return;
//     }
//     throw error;
//   }

//   res
//     .status(200)
//     .send({ message: 'Success' });
// };

// const loginUser = async (req, res) => {
//   const { email, password } = req.body;

//   const user = await User.findOne({ email });

//   if (!user) {
//     res
//       .status(400)
//       .json({ message: 'Username or password is invalid' });
//     return;
//   }

//   const isPasswordCorrect = await bcrypt.compare(password, user.password);

//   if (!isPasswordCorrect) {
//     res
//       .status(400)
//       .json({ message: 'Username or password is invalid' });

//     return;
//   }

//   const payload = { email: user.email, userId: user._id };
//   const jwtToken = jwt.sign(payload, process.env.JWT_KEY);

//   return res
//     .status(200)
//     .send({
//       message: 'Success',
//       jwt_token: jwtToken,
//     });
// };

// module.exports = {
//   registerUser,
//   loginUser,
// };
