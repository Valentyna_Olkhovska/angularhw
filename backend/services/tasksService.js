const bcrypt = require('bcryptjs');
const { Types } = require('mongoose');
const { Board } = require('../models/Board.js');
const { Task, Archive, TaskJoiSchema } = require('../models/Tasks.js');

const addTask = async (req, res) => {
  // const { userId } = req.user;
  const { id } = req.params;
  const { name, description, status } = req.body;
  await TaskJoiSchema.validateAsync({ name, description, status });

  const task = new Task({
    name,
    description,
    status,
    board: id,
    createdDate: new Types.ObjectId().getTimestamp().toISOString().replace(/T/, ' ').replace(/\..+/, ''),
  });
  const board = await Board.findById({ _id: id });

  board.tasks.push({ name, taskId: task._id, status })

  await board.save()

  try {
    await task.save();
  } catch (error) {
    throw error;
  }

  res
    .status(200)
    .send({ message: 'Task created successfully' });
};

const getTaskById = async (req, res) => {
  const { taskId } = req.params;

  const task = await Task.findById({ _id: taskId });

  res
    .status(200)
    .send({
      task,
    });

};

const updateTask = async (req, res) => {
  const { taskId } = req.params;
  const { name, description } = req.body;

  if (!name && !description) {
    res
      .status(404)
      .send({ message: 'Nothing to update' });
  }

  const task = await Task.findById({ _id: taskId });


  if (!task) {
    res
      .status(404)
      .send({ message: 'Nothing to update' });
  }


  const taskName = name.length >= 3 ? name : task.name;
  const taskDescription = description.length >= 3 ? description : task.description;
  const status = task.status

  const newTask = await Task.findByIdAndUpdate({ _id: taskId }, { name: taskName, description: taskDescription, status }, { returnDocument: 'after' });


  await TaskJoiSchema.validateAsync({ name: taskName, description: taskDescription, status });

  try {
   
    console.log(newTask)

    await Board.updateOne({ 'tasks.taskId': taskId }, { '$set': { 'tasks.$.name': name } });


    res
      .status(200)
      .send({ message: 'Task update successfully', newTask });
  } catch (error) {
    throw error;
  }


};

const updateTaskStatus = async (req, res) => {
  const { taskId } = req.params;
  const { status } = req.body;

  const task = await Task.findById({ _id: taskId });

  const board = await Board.findOneAndUpdate({ 'tasks.taskId': taskId }, { '$set': { 'tasks.$.status': status } });


  if (!task) {
    return res
      .status(400)
      .send({ message: 'Task not found' });
  }

  const name = task.name;
  const description = task.description;

  await TaskJoiSchema.validateAsync({ status, name, description });


  try {
    await task.updateOne({
      status
    });

    res
      .status(200)
      .send({ message: 'Task status update successfully' });

  } catch (error) {
    console.log(error)
  }

};


const deleteTask = async (req, res) => {
  const { taskId } = req.params;

  if (!taskId) {
    res
      .status(404)
      .send({
        message: 'Nothing to delete!',
      });
  }

  const task = await Task.findById({ _id: taskId });

  if (!task) {
    res
      .status(404)
      .send({
        message: 'Not found!',
      });
  }

  task.delete();

  await Board.updateOne({ 'tasks.taskId': taskId }, { '$pull': { "tasks": { 'taskId': taskId } } });

  res
    .status(200)
    .send({
      message: 'Task deleted successfully!',
    });
};

module.exports = {
  addTask,
  getTaskById,
  updateTask,
  updateTaskStatus,
  deleteTask,
};
