const bcrypt = require('bcryptjs');
const { Types } = require('mongoose');
const { Board, BoardJoiSchema, EditBoardSchema } = require('../models/Board.js');
const { Task } = require('../models/Tasks.js');



const getBoards = async (req, res) => {
  const boards = await Board.find({});

  if (!boards) {
    res
      .status(400)
      .send({ message: 'Boards not found' });

    return;
  }

  res
    .status(200)
    .send({ boards });
};

const addBoard = async (req, res) => {
  // const { userId } = req.user;
  const { name, description } = req.body;

  await BoardJoiSchema.validateAsync({ name, description });

  const board = new Board({
    name,
    description,
    // created_by: userId,
    createdDate: new Types.ObjectId().getTimestamp().toISOString().replace(/T/, ' ').replace(/\..+/, ''),
  });

  try {
    await board.save();
  } catch (error) {
    throw error;
  }

  res
    .status(200)
    .send({ message: 'Board created successfully', board });
};

const getBoardById = async (req, res) => {
  const { id } = req.params;

  const board = await Board.findById({ _id: id });


  res
    .status(200)
    .send({
      board,
    });

};

const getBoardTasks = async (req, res) => {
  const { id } = req.params;

  // const board = await Board.findById({ _id: id });

  const tasks = await Task.find({ board: id });

  res
    .status(200)
    .send({
      // board,
      tasks
    });

};

const updateBoard = async (req, res) => {
  const { id } = req.params;
  const { name } = req.body;

  await EditBoardSchema.validateAsync({ name });

  const board = await Board.findByIdAndUpdate({ _id: id }, { name }, { returnDocument: 'after' });

  res
    .status(200)
    .send({ message: 'Board update successfully', board });
};

const deleteBoard = async (req, res) => {
  const { id } = req.params;

  const board = await Board.findById({ _id: id });
  board.delete();

  res
    .status(200)
    .send({
      message: 'Board deleted successfully!', board,
    });
};





module.exports = {
  getBoards,
  getBoardTasks,
  getBoardById,
  addBoard,
  updateBoard,
  deleteBoard,

};
