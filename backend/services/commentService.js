const bcrypt = require('bcryptjs');
const { Types } = require('mongoose');
const { Comment, CommentJoiSchema } = require('../models/Comments');
const { Task} = require('../models/Tasks.js');

const addComment = async (req, res) => {
    const { id } = req.params;
    const { name, text } = req.body;
    await CommentJoiSchema.validateAsync({ name, text });

    const comment = new Comment({
        name,
        text,
        task: id,
        createdDate: new Types.ObjectId().getTimestamp().toISOString(),
    });
    try {
        await comment.save();
      } catch (error) {
        throw error;
      }
    res
        .status(200)
        .send({ message: 'Comment created successfully', comment });
};

const getAllComments = async (req, res) => {
    const { id } = req.params;

    const comments = await Comment.find({ task: id });

    res
        .status(200)
        .send({
            comments,
        });

};

const getCommentTask = async (req, res) => {
    const { id } = req.params;

    const task = await Task.findById({ _id: id });

    res
        .status(200)
        .send({
            task,
        });
};



module.exports = {
    getCommentTask,
    getAllComments,
    addComment
};
