import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Task } from '../core/models/task.model';
import { BaseApi } from './base.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};

@Injectable()
export class TaskDetailsService extends BaseApi {
  homeUrl = `${this.hostUrl}/api/task`;

  constructor(private http: HttpClient) { 
    super();
  }

  getTaskById(taskId: string) {
    const url = `${this.homeUrl}/${taskId}`;

    return this.http.get<{task: Task }>(url)
  }


}
