import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { Board } from '../core/models/dashboard.model';
import { Task } from '../core/models/task.model';
import { Comment } from '../core/models/comment.model';
import { BaseApi } from './base.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};

@Injectable()
export class CommentService extends BaseApi {
  homeUrl = `${this.hostUrl}/api/task`;

  constructor(private http: HttpClient) { 
    super();
  }

  getAllComments(id: string) {
    const url = `${this.homeUrl}/${id}/comments`;

    return this.http.get<{comments: Comment[] }>(url)
  };

  addComment(comment: Omit<Comment, '_id' | 'createdDate' >, id:string) {

    const url = `${this.homeUrl}/${id}`;
    return this.http.post<{ comment: Comment }>(url, comment, httpOptions);
  };


//   editBoard(board: Omit<Board,'createdDate' | 'tasks'>) {
//     const url = `${this.homeUrl}/${board._id}`;
//     return this.http.patch<{ board: Board }>(url, board, httpOptions);
//   }

//   deleteBoard(board: Omit<Board,'createdDate' | 'tasks'>) {
//     const url = `${this.homeUrl}/${board._id}`;
//     return this.http.delete<{ board: Board }>(url);


//   }
}
