import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Task } from '../core/models/task.model';
import { BaseApi } from './base.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};

@Injectable()
export class TaskService extends BaseApi {
  homeUrl = `${this.hostUrl}/api/board`;

  constructor(private http: HttpClient) { 
    super();
  }

  getTasks(id: string) {
    const url = `${this.homeUrl}/tasks/${id}`;

    return this.http.get<{tasks: Task[] }>(url)

  };

  addTask(task: Omit<Task, '_id' | 'createdDate' >, id:string) {
    
    const url = `${this.homeUrl}/${id}/task/create`;
    return this.http.post<{ task: Task }>(url, task, httpOptions);
  };

  setTaskStatus(taskId: string, nextStatus: string) {
    return this.http.patch<{ task: Task }>(`${this.homeUrl}/task/${taskId}`, {
      status: nextStatus
    }, httpOptions);
  }

  getTaskById(taskId: string) {
    const url = `${this.homeUrl}/${taskId}`;

    return this.http.get<{task: Task }>(url)
  }

  editTask(task: Omit<Task,'createdDate'| 'board' | 'status'>) {
    const url = `${this.homeUrl}/task/${task._id}`;
    return this.http.put<{ task: Task }>(url, task, httpOptions);
  }

  deleteTask(task: Omit<Task,'createdDate' | 'board' | 'status' | 'name' | 'description'>) {
    const url = `${this.homeUrl}/task/${task._id}`;
    return this.http.delete<{ task: Task }>(url);


  }
}
