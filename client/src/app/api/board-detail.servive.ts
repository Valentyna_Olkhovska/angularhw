import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BaseApi } from './base.service';
import { Board } from '../core/models/dashboard.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};

@Injectable()
export class BoardDetailsService extends BaseApi {
  homeUrl = `${this.hostUrl}/api/board`;

  constructor(private http: HttpClient) { 
    super();
  }

  getBoardById(taskId: string) {
    const url = `${this.homeUrl}/${taskId}`;

    return this.http.get<{board: Board }>(url)
  }


}
