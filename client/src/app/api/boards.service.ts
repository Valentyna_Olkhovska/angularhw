import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Board } from '../core/models/dashboard.model';
import { BaseApi } from './base.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};

@Injectable()
export class BoardService extends BaseApi {
  homeUrl = `${this.hostUrl}/api/board`

  constructor(private http: HttpClient) {
    super();
   }

  getBoards() {
    return this.http.get<{ boards: Board[] }>(this.homeUrl)
  };

  addBoard(board: Omit<Board, '_id' | 'createdDate' | 'tasks'>) {
    return this.http.post<{ board: Board }>(this.homeUrl, board, httpOptions);
  };

  editBoard(board: Omit<Board,'createdDate' | 'tasks'>) {
    const url = `${this.homeUrl}/${board._id}`;
    return this.http.patch<{ board: Board }>(url, board, httpOptions);
  }

  deleteBoard(board: Omit<Board,'createdDate' | 'tasks'>) {
    const url = `${this.homeUrl}/${board._id}`;
    return this.http.delete<{ board: Board }>(url);
  }
}
