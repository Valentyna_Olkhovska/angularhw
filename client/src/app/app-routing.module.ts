import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BoardComponent } from './board-page/board/board.component';
import { NewTaskFormComponent } from './board-page/new-task-form/new-task-form.component';
import { TaskArchiveComponent } from './board-page/task-archive/task-archive.component';
import { TaskEditComponent } from './board-page/task-edit/task-edit.component';
import { EditBoardFormComponent } from './home-page/edit-board-form/edit-board-form.component';
import { HomeComponent } from './home-page/home/home.component';
import { NewBoardFormComponent } from './home-page/new-board-form/new-board-form.component';
import { TaskDetailsComponent } from './task-page/task-details/task-details.component';

const routes: Routes = [
  {
    path: 'home', component: HomeComponent,
    children: [{
      path: 'new',
      component: NewBoardFormComponent
    },
    {
      path: 'edit/:id',
      component: EditBoardFormComponent
    }
    ]
  },
  {
    path: 'board/:id',
    component: BoardComponent,
    children: [
      {
        path: 'new/:status',
        component: NewTaskFormComponent
      },
      {
        path: 'task/edit/:id',
        component: TaskEditComponent
      },
      {
        path: 'task/archive/:id',
        component: TaskArchiveComponent
      }

    ]
  },
  { path: 'task/:id', component: TaskDetailsComponent },
  {
    path: '', redirectTo: '/home', pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
