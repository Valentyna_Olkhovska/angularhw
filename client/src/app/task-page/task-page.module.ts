import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../app-routing.module';
import { AppComponent } from '../app.component';
import { TaskDetailsComponent } from './task-details/task-details.component';
import { TaskCommentComponent } from './task-comment/task-comment.component';
import { CommentComponent } from './comment/comment.component';
import { CommentService } from '../api/comment.service';
import { FormsModule } from '@angular/forms';
import { TaskDetailsService } from '../api/task-details.service';



@NgModule({
    declarations: [
        TaskDetailsComponent,
        TaskCommentComponent,
        CommentComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule
    ],
    providers: [CommentService, TaskDetailsService],
    bootstrap: [AppComponent]
})
export class TaskPageModule { }
