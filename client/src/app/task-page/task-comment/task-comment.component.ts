import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TaskCommentStateService } from 'src/app/core/state/taskComment';
import { Store, select } from "@ngrx/store"
import { AppRootState } from 'src/app/app.module';
import { selectComments } from 'src/app/core/state/taskComment/taskComment.selector';
import { Comment } from 'src/app/core/models/comment.model';



@Component({
  selector: 'app-task-comment',
  templateUrl: './task-comment.component.html',
  styleUrls: ['./task-comment.component.css']
})
export class TaskCommentComponent implements OnInit {

  id: string | undefined
  name = ''
  text = ''

  comments: Comment[] = []

  constructor(
    private route: ActivatedRoute,
    private store: Store<AppRootState>,
    private taskCommentStateService: TaskCommentStateService,

  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(({ id }) => {
      this.id = id;

    });

    this.getComments();

    this.store.select(selectComments).subscribe((comments) => {
      this.comments = comments;

    });

  }

  addComment(): void {
    if (!this.id) {
      throw new Error('Id is not found');
    }
    this.taskCommentStateService.addNewComment({
      name: this.name,
      text: this.text,
      task: this.id,

    }, this.id);
  }

  add(name: string, text: string): void {

    name = name.trim();
    text
    if (!name || !text) {
      return;
    }

    this.addComment();
  }

  getComments() {

    if (!this.id) {
      throw new Error('Id is not found');
    }

    this.taskCommentStateService.getAllComments(this.id);
  }

}
