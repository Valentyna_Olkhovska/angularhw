import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppRootState } from 'src/app/app.module';
import { Store, select } from "@ngrx/store"
import * as selectors from 'src/app/core/state/board/tasks.selector';
import { Task } from 'src/app/core/models/task.model';
import { TaskCommentStateService } from 'src/app/core/state/taskComment';
import { TaskDetailsService } from 'src/app/api/task-details.service';

@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.css'],
  providers: [TaskCommentStateService, TaskDetailsService],


})
export class TaskDetailsComponent implements OnInit {


  id: string = ''
  taskName: string = ''
  description?: string = ''
  createdDate?: Date | undefined = undefined
  boardId: string = ''


  constructor(
    private route: ActivatedRoute,
    private taskDetailsService: TaskDetailsService

  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(({ id }) => {
      this.id = id;
    });

    this.getTaskDetails()

  }

  getTaskDetails() {

    return this.taskDetailsService.getTaskById(this.id).subscribe(({ task }) => {
      this.taskName = task.name;
      this.description = task.description;
      this.createdDate=task.createdDate
      this.boardId = task.board

    });

  }






}
