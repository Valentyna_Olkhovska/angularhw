import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  @Input() name: string = ''
  @Input() text: string = ''
  @Input() date: Date | string = ''



  constructor() { }

  ngOnInit(): void {
  }

}
