import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HomeModule } from './home-page/home.module'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BoardModule } from './board-page/board.module';
import { TaskPageModule } from './task-page/task-page.module';
// import { HomeService } from './home-page/home-service';
import { HttpClientModule } from '@angular/common/http';

import { StoreModule } from '@ngrx/store';
import{dashboardReducer} from "./core/state/dashboard/dashboard.reducer";
import{TaskReducer} from "./core/state/board/tasks.reducer";

import { DashboardsState } from './core/state/dashboard/dashboard.state';
import { BoardsState } from './core/state/board/tasks.state';
import { CommentState } from './core/state/taskComment/taskComment.state';
import { CommentReducer } from './core/state/taskComment/task.Comment.reducer';

export interface AppRootState { 
  boardsState: DashboardsState,
  tasksState: BoardsState,
  commentsStatate: CommentState
}

@NgModule({
  declarations: [
    AppComponent,
    // SortPipe,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    HomeModule,
    BoardModule,
    TaskPageModule,
    StoreModule.forRoot<AppRootState>({ boardsState: dashboardReducer, tasksState: TaskReducer,   commentsStatate: CommentReducer})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
