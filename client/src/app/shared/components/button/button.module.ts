import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../../../app-routing.module';
import { AppComponent } from '../../../app.component';
import { ButtonComponent } from './button.component';


@NgModule({
    declarations: [
        ButtonComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,

    ],
    exports: [
        ButtonComponent

    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class ButtonModule { }
