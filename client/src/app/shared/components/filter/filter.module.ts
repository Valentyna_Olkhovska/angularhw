import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../../../app-routing.module';
import { AppComponent } from '../../../app.component';
import { FilterComponent } from './filter.component';


@NgModule({
    declarations: [
        FilterComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,

    ],
    exports: [
        FilterComponent

    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class FilterModule { }
