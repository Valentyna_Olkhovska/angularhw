import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../../../app-routing.module';
import { AppComponent } from '../../../app.component';
import { SortComponent } from './sort.component';



@NgModule({
    declarations: [
        SortComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,

    ],
    exports: [
        SortComponent

    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class SortModule { }
