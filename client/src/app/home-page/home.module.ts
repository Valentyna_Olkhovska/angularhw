import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../app-routing.module';
import { AppComponent } from '../app.component';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { DashboardCreateComponent } from './dashboard-create/dashboard-create.component';
import { ButtonModule } from '../shared/components/button/button.module';
import { FilterModule } from '../shared/components/filter/filter.module';
import { SortModule } from '../shared/components/sort/sort.module';
import { NewBoardFormComponent } from './new-board-form/new-board-form.component';

import { HttpClientModule } from '@angular/common/http';
import { BoardService } from '../api/boards.service';
import { EditBoardFormComponent } from './edit-board-form/edit-board-form.component';
import { SortBoardsPipe } from './pipe/sort-boards.pipe';
import { FilterBoardsPipe } from './pipe/filter-boards.pipe';

@NgModule({
  declarations: [
    HomeComponent,
    DashboardCreateComponent,
    NewBoardFormComponent,
    EditBoardFormComponent,
    SortBoardsPipe,
    FilterBoardsPipe

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ButtonModule,
    FilterModule,
    SortModule,
  ],
  providers: [BoardService],
  bootstrap: [AppComponent]
})
export class HomeModule { }
