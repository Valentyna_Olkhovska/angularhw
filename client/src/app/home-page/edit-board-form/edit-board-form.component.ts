
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from "@ngrx/store"
import { Board } from 'src/app/core/models/dashboard.model';
import { DashboardStateService } from 'src/app/core/state/dashboard';

@Component({
  selector: 'app-edit-board-form',
  templateUrl: './edit-board-form.component.html',
  styleUrls: ['./edit-board-form.component.css'],
  providers: [DashboardStateService],
})
export class EditBoardFormComponent implements OnInit {

  editBoard: Board | undefined;
  name = ''
  description = ''

  id?: string

  constructor(
    private store: Store,
    private DashboardStateService: DashboardStateService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(({ id }) => {
      this.id = id;

    });
  }

  updateBoard() {
    if (!this.id) {
      throw new Error('Id is not found');
    }

    this.DashboardStateService.updateBoard({
      _id: this.id,
      name: this.name
    });
  }

  update(name: string) {
    this.editBoard = undefined;
    name = name.trim();
    if (!name) {
      return
    }
    this.updateBoard()
  }

}
