import { Pipe, PipeTransform } from '@angular/core';
import { Board } from 'src/app/core/models/dashboard.model';

@Pipe({
  name: 'filter'
})
export class FilterBoardsPipe implements PipeTransform {

  transform(boards: Board[], filterBy: string): Board[] {
    
    if (!filterBy) {
      return boards
    }

    return boards.filter(b =>
      b.name.toLowerCase().includes(filterBy.toLowerCase())
      ||
      b.tasks.find(t => t.name.toLowerCase().includes(filterBy.toLowerCase())))

  }

}
