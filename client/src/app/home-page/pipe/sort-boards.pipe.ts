import { Pipe, PipeTransform } from '@angular/core';
import { Board } from 'src/app/core/models/dashboard.model';

@Pipe({
  name: 'sort'
})

export class SortBoardsPipe implements PipeTransform {

  transform(boards: Board[], sortField: [string, boolean]): any {
    const sortBy = sortField[0]
    const sortAsc = sortField[1]

    if (!boards || !sortBy) {
      return boards
    }

    let sorted: Board[];
    switch (sortBy) {
      case 'Name':

        sorted = [...boards].sort((a: Board, b: Board) => this.sortByName(a, b))


        break;
      case 'Date of creation':

        sorted = [...boards].sort((a: Board, b: Board) => this.sortByDate(a, b)).reverse()

        break

      case 'Number of To Do task':
        sorted = [...boards].sort((a: Board, b: Board) => this.sortByStatus(a, b, 'ToDo'))
        break

      case 'Number of in progress task':
        sorted = [...boards].sort((a: Board, b: Board) => this.sortByStatus(a, b, 'InProgress'))
        break

      case 'Number of done task':
        sorted = [...boards].sort((a: Board, b: Board) => this.sortByStatus(a, b, 'Done'))
        break

      default: sorted = boards;
        break
    }

    if (!sortAsc) {
      return sorted.reverse()
    }
    return sorted
  }

  sortByName(a: Board, b: Board) {
    if (a.name < b.name) {
      return -1
    } else if (a.name > b.name) {
      return 1
    }
    return 0
  }
  sortByDate(a: Board, b: Board) {
    return +new Date(a.createdDate) - +new Date(b.createdDate);

  }
  sortByStatus(a: Board, b: Board, status: string) {
    const aNumberOfStatus = a.tasks.filter(item => item.status === status).length
    const bNumberOfStatus = b.tasks.filter(item => item.status === status).length
   
    return aNumberOfStatus - bNumberOfStatus
  }

}

