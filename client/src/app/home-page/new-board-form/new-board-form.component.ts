import { formatDate } from '@angular/common';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from "@ngrx/store"
import { Board } from 'src/app/core/models/dashboard.model';
import { DashboardStateService } from 'src/app/core/state/dashboard';

@Component({
  selector: 'app-new-board-form',
  templateUrl: './new-board-form.component.html',
  styleUrls: ['./new-board-form.component.css'],
  providers: [DashboardStateService],
})
export class NewBoardFormComponent implements OnInit {

  boards: Board[] = []


  editBoard: Board | undefined;
  name = ''
  description = ''

  constructor(
    private store: Store,
    private DashboardStateService: DashboardStateService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
  }

  addBoard(): void {
    this.DashboardStateService.addNewBoard({
      name: this.name,
      description: this.description
    });

  }

  add(name: string, description: string): void {
    this.editBoard = undefined;
    name = name.trim();
    description
    if (!name || !description) {
      return;
    }
    this.addBoard();
  }

}
