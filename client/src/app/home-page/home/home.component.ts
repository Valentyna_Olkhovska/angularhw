import { Component, OnInit } from '@angular/core';
import { Store, select } from "@ngrx/store"
import { Board } from 'src/app/core/models/dashboard.model';
import { selectBoards } from 'src/app/core/state/dashboard/dashboard.selector';


import { DashboardStateService } from 'src/app/core/state/dashboard';
import { AppRootState } from 'src/app/app.module';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [DashboardStateService],

})
export class HomeComponent {

  sortBy= ''
  sortAsc = true
  filterBy = ''

  boards: Board[] = []


  sortArray = [ { name: 'Name' },{ name: 'Date of creation' },
  { name: 'Number of To Do task' }, { name: 'Number of in progress task' }, { name: 'Number of done task' }];

  public ascButton = "ASC";
  public descButton = "DESC";


  constructor(
    private store: Store<AppRootState>,
    private dashboardStateService: DashboardStateService
  ) { }


  ngOnInit(board: Board): void {
    this.getBoards();

    this.store.select(selectBoards).subscribe((boards) => {
      this.boards = boards;

    });


  }

  getBoards() {
    this.dashboardStateService.fetchBoards();
  }

  delete(id: string, name: string) {
    if (!id) {
      throw new Error('Id is not found');
    }

    this.dashboardStateService.deleteBoard({
      _id: id,
      name
    });

  }

  calcTasks(board : Board, status: string){
    return board.tasks.filter(item => item.status === status).length
  }


  clickHandler(value: string): void {
    console.log(value)

  }

}


