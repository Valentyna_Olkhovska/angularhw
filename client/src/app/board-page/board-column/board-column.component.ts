import { Component, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Task } from 'src/app/core/models/task.model';
import { TaskStateService } from 'src/app/core/state/board';

@Component({
  selector: 'app-board-column',
  templateUrl: './board-column.component.html',
  styleUrls: ['./board-column.component.css'],
  providers: [TaskStateService]
})
export class BoardColumnComponent implements OnInit {
  @Input() tasks: Task[] = [];
  @Input() name: string = ''
  @Input() status: string = ''
  @Input() filterBy: string = ''
  @Input() sortBy: string = ''
  @Input() sortAsc: boolean = true

  columnColor: string = "#D9D9D9";

  constructor(
    private route: ActivatedRoute,
    private taskStateService: TaskStateService,
  ) { }

  ngOnInit() {

    this.route.params.subscribe(({ id }) => {
    });

    this.columnColor = localStorage.getItem(`${this.name}ColumnColor`) || "#D9D9D9"
  }

  onChange(event: any) {
    this.columnColor = event.currentTarget.value;
    localStorage.setItem(`${this.name}ColumnColor`, `${event.currentTarget.value}`);

  }

  getColor(){
    
    this.columnColor = localStorage.getItem(`${this.name}ColumnColor`) || "#D9D9D9"
  }
 
  dropHandler(event: any): void {
    const currentColumnStatus = event.currentTarget.getAttribute('data-column-status');

    try {
      const {
        taskId,
        parentColumnStatus
      } = JSON.parse(event.dataTransfer.getData("text/plain"));

      if (currentColumnStatus === parentColumnStatus) {
        return;
      }

      this.taskStateService.setTaskStatus(taskId, currentColumnStatus);
    } catch (error) {
      alert('Somesthing whent wrong');
      console.log(error);
    }

  }

  dragoverHandler(event: any): void {
    event.preventDefault();
  }

  dragStartHandler(event: any): void {
    event.dataTransfer.setData("text/plain", JSON.stringify({
      taskId: event.currentTarget.getAttribute('data-task-id'),
      parentColumnStatus: event.currentTarget.getAttribute('data-column-status'),
    }));

    event.currentTarget.style = 'opacity: 0.4';
  }

  dragEndHandler(event: any): void {
    event.currentTarget.style = 'opacity: 1';
  }
}



