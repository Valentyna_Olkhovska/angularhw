import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../app-routing.module';
import { AppComponent } from '../app.component';
import { ButtonModule } from '../shared/components/button/button.module';
import { FilterModule } from '../shared/components/filter/filter.module';
import { SortModule } from '../shared/components/sort/sort.module';
import { BoardColumnComponent } from './board-column/board-column.component';
import { BoardComponent } from './board/board.component';
import { TaskCreateComponent } from './task-create/task-create.component';
import { NewTaskFormComponent } from './new-task-form/new-task-form.component';
import { TaskComponent } from './task/task.component';
import { TaskService } from '../api/task.service';
import { FormsModule } from '@angular/forms';
import { SortTasksPipe } from './pipe/sort-tasks.pipe';
import { FilterTasksPipe } from './pipe/filter-tasks.pipe';
import { TaskEditComponent } from './task-edit/task-edit.component';
import { TaskArchiveComponent } from './task-archive/task-archive.component';
import { BoardDetailsService } from '../api/board-detail.servive';
// import { DragableDirective } from './dragable/dragable.directive';
// import { DragableModule } from './dragable/dragable.module';
// import { DragableDirective } from './dragable/dragable.directive';

@NgModule({
    declarations: [
        BoardComponent,
        BoardColumnComponent,
        TaskCreateComponent,
        NewTaskFormComponent,
        TaskComponent,
        SortTasksPipe,
        FilterTasksPipe,
        TaskEditComponent,
        TaskArchiveComponent,
        // DragableDirective
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        ButtonModule,
        FilterModule,
        SortModule,
        FormsModule,



    ],
    providers: [TaskService, BoardDetailsService],
    bootstrap: [AppComponent]
})
export class BoardModule { }