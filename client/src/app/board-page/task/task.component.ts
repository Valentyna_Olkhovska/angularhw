import { Component, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppRootState } from 'src/app/app.module';
import { Task } from 'src/app/core/models/task.model';
import { TaskStateService } from 'src/app/core/state/board';
import { Store, select } from "@ngrx/store"


@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  @Input()
  taskName: string = '';
  @Input()
  taskStatus: string = '';
  @Input()
  description: string | undefined = '';
  @Input()
  createdDate: Date | undefined = undefined;
  @Input()
  id: string = '';
  @Input()
  archived: boolean = false

  constructor(private _router: Router,
    private taskStateService: TaskStateService,
    private store: Store<AppRootState>,

  ) { }

  ngOnInit(): void {

  }


  delete() {
    if (!this.id) {
      throw new Error('Id is not found');
    }

    this.taskStateService.deleteTask({
      _id: this.id
    });
  }

  archive() {

    if (this.taskStatus === 'Archived') {
      return this.taskStateService.setTaskStatus(this.id, 'ToDo');

    }
    return this.taskStateService.setTaskStatus(this.id, 'Archived');

  }


}
