import { Component, OnInit, Output } from '@angular/core';
import { Board } from 'src/app/core/models/dashboard.model';
import { Store, select } from "@ngrx/store"

import { TaskStateService } from 'src/app/core/state/board/taskState.service';

import { AppRootState } from 'src/app/app.module';
import { selectTasks } from 'src/app/core/state/board/tasks.selector';


import { Task } from 'src/app/core/models/task.model';
import { ActivatedRoute } from '@angular/router';
import { BoardDetailsService } from 'src/app/api/board-detail.servive';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css'],
  providers: [TaskStateService, BoardDetailsService],


})
export class BoardComponent implements OnInit {

  sortBy = ''
  filterBy = ''
  sortAsc = true

  sortArray = [{ name: 'Task name' }, { name: 'Date of creation' }];

  public ascButton = "ASC";
  public descButton = "DESC";

  tasks: Task[] = []

  id?: string
  task: any

  boardName?: string = ''
  description?: string = ''
  createdDate?: Date | undefined = undefined


  constructor(
    private store: Store<AppRootState>,
    private taskStateService: TaskStateService,
    private boardDetailsService: BoardDetailsService,
    private route: ActivatedRoute,


  ) { }

  ngOnInit(): void {

    this.route.params.subscribe(({ id }) => {
      this.id = id;

    });

    this.getTasks();

    this.getBoardDetails()

    this.store.select(selectTasks).subscribe((tasks) => {
      this.tasks = tasks;

    });


  }

  getTasks() {

    if (!this.id) {
      throw new Error('Id is not found');
    }

    this.taskStateService.fetchTasks(this.id);
  }

  getBoardDetails() {
    if (!this.id) {
      throw new Error('Id is not found');

    }

    return this.boardDetailsService.getBoardById(this.id).subscribe(({ board }) => {
      this.boardName = board.name;
      this.description = board.description;
      this.createdDate = board.createdDate

    });
  }


  returnTaskByStatus(status: string) {

    return this.tasks.filter(t => t?.status === status)
  }

}
