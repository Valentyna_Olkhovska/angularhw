import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Task } from 'src/app/core/models/task.model';
import { TaskStateService } from 'src/app/core/state/board';
import { Store, select } from "@ngrx/store"
import { AppRootState } from 'src/app/app.module';
import { selectTasks } from 'src/app/core/state/board/tasks.selector';


@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: ['./task-edit.component.css']
})
export class TaskEditComponent implements OnInit {

  boardId: string = ''
  taskId : string = ''
  name: string = ''
  description: string = ''
  tasks: Task[] = [];

  constructor(
    private taskStateService: TaskStateService,
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<AppRootState>,

  ) { }

  ngOnInit(): void {
    this.route.parent?.params.subscribe(({ id }) => {
      this.boardId = id;
    });

    this.route.params.subscribe(({ id }) => {
      this.taskId = id;

    });



  }

  updateBoard() {
    if (!this.taskId) {
      throw new Error('Id is not found');
    }

    this.taskStateService.updateTask({
      _id: this.taskId,
      name: this.name,
      description: this.description
    });
  }

  update(name: string, description: string) {
    name = name.trim();
    description = description.trim();
    if (!name && !description) {
      return
    }
    this.updateBoard()
    this.goRoute()

  }


  goRoute(): void {

    this.router.navigateByUrl(`/board/${this.boardId}`);

  }

}
