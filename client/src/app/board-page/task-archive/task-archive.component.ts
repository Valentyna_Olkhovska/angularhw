import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Task } from 'src/app/core/models/task.model';
import { TaskStateService } from 'src/app/core/state/board';
import { Store, select } from "@ngrx/store"
import { AppRootState } from 'src/app/app.module';
import { selectTasks } from 'src/app/core/state/board/tasks.selector';

@Component({
  selector: 'app-task-archive',
  templateUrl: './task-archive.component.html',
  styleUrls: ['./task-archive.component.css']
})
export class TaskArchiveComponent implements OnInit {

  id: string = ''
  tasks: Task[] = [];

  constructor(
    private taskStateService: TaskStateService,
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<AppRootState>,

  ) { }

  ngOnInit(): void {
    this.route.parent?.params.subscribe(({ id }) => {
      this.id = id;
    });

    this.getTasks()

    this.store.select(selectTasks).subscribe((tasks) => {
      this.tasks = tasks.filter(t => t?.status === 'Archived');

    });

  }

  getTasks() {

    if (!this.id) {
      throw new Error('Id is not found');
    }

    this.taskStateService.fetchTasks(this.id);

  }


  goRoute(): void {

    this.router.navigateByUrl(`/board/${this.id}`);

  }

}


