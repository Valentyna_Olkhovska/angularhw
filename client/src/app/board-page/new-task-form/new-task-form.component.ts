import { Component, OnInit, Input } from '@angular/core';
import { Task } from 'src/app/core/models/task.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from "@ngrx/store"
import { TaskStateService } from 'src/app/core/state/board';



@Component({
  selector: 'app-new-task-form',
  templateUrl: './new-task-form.component.html',
  styleUrls: ['./new-task-form.component.css'],
  providers: [TaskStateService],

})
export class NewTaskFormComponent implements OnInit {

  tasks: Task[] = []


  editTask: Task | undefined;
  name = ''
  description = ''

  id?: string
  status?: string


  constructor(
    private store: Store,
    private TaskStateService: TaskStateService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.parent?.params.subscribe(({ id }) => {
      this.id = id;
    });
    this.route.params.subscribe(({ status }) => {
      this.status = status

    });

  }

  addTask(): void {

    if (!this.id || !this.status) {
      throw new Error('Id is not found');
    }
    this.status = this.status.replace(/\s/g, '')
    console.log(this.id)
    console.log(this.status.replace(/\s/g, ''))

    this.TaskStateService.addNewTask({
      name: this.name,
      description: this.description,
      board: this.id,
      status: this.status.replace(/\s/g, '')

    }, this.id);
  }

  add(name: string, description: string): void {

    console.log(name, description)

    this.editTask = undefined;
    name = name.trim();
    description
    if (!name || !description) {
      return;
    }

    this.addTask();
    this.goRoute()
  }

  goRoute(): void {

    this.router.navigateByUrl(`/board/${this.id}`);

  }


}
