import { Pipe, PipeTransform } from '@angular/core';
import { Board } from 'src/app/core/models/dashboard.model';
import { Task } from 'src/app/core/models/task.model';

@Pipe({
  name: 'filter'
})
export class FilterTasksPipe implements PipeTransform {

  transform(tasks: Task[], filterBy: string): Task[] {

    if (!filterBy) {
      return tasks
    }

    return tasks.filter(b =>
      b.name.toLowerCase().includes(filterBy.toLowerCase()))

  }

}
