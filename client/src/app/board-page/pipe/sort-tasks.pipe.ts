import { Pipe, PipeTransform } from '@angular/core';
import { Task } from 'src/app/core/models/task.model';


@Pipe({
  name: 'sort'
})

export class SortTasksPipe implements PipeTransform {

  transform(tasks: Task[], sortField: [string, boolean]): any {
    const sortBy = sortField[0]
    const sortAsc = sortField[1]

    if (!tasks || !sortBy) {
      return tasks
    }

    let sorted: Task[];
    switch (sortBy) {
      case 'Name':
        // sortAsc ?
        sorted = [...tasks].sort((a: Task, b: Task) => this.sortByName(a, b))
        // : sorted = [...boards].sort((a: Board, b: Board) => this.sortByName(a, b)).reverse();

        break;
      case 'Date of creation':
        // sortAsc ?
        sorted = [...tasks].sort((a: Task, b: Task) => this.sortByDate(a, b)).reverse()
        // : sorted = [...boards].sort((a: Board, b: Board) => this.sortByDate(a, b));
        break

      default: sorted = tasks;
        break
    }

    if (!sortAsc) {
      return sorted.reverse()
    }
    return sorted
  }

  sortByName(a: Task, b: Task) {
    if (a.name < b.name) {
      return -1
    } else if (a.name > b.name) {
      return 1
    }
    return 0
  }
  sortByDate(a: Task, b: Task) {
    return +new Date(a.createdDate) - +new Date(b.createdDate);

  }

}

