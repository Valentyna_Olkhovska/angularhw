import{Comment} from '../../models/comment.model'

export interface CommentState {
    comments: Comment[]
}

export const initialState: CommentState = {
    comments: []
}