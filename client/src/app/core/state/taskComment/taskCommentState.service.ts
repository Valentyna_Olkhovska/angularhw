import { Injectable } from '@angular/core';
import { CommentService } from "src/app/api/comment.service";
import { AppRootState } from "src/app/app.module";
import { Store } from "@ngrx/store"
import {  getAllComments,addTaskComment } from './taskComment.action';
import { Comment } from '../../models/comment.model';

@Injectable()
export class TaskCommentStateService {
    constructor(
        private store: Store<AppRootState>,
        private CommentService: CommentService
    ) { }


    async getAllComments(id: string): Promise<void> {

        this.CommentService.getAllComments(id)
            .subscribe(({ comments }) => {
                this.store.dispatch(getAllComments({ comments }));
            });

    }

    async addNewComment(newComment: Omit<Comment, '_id' | 'createdDate'>, id: string): Promise<void> {
        this.CommentService.addComment(newComment, id)
        .subscribe(({ comment }) => {

            this.store.dispatch(addTaskComment({ 
                comment: {
                    ...comment,
                    ...newComment,
                }
             }))
        })
    }


}