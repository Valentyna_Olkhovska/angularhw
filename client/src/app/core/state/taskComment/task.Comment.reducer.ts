import { createReducer, on } from "@ngrx/store"
import { initialState } from "./taskComment.state"
import * as TaskAction from "./taskComment.action"



export const CommentReducer = createReducer(
    initialState,

    on(TaskAction.getAllComments, (state, {comments}) => {
        
        return {
            ...state,
            comments
        }
    }),
    on(TaskAction.addTaskComment, (state, { comment }) => {
        return {
            ...state,
            comments: [
                ...state.comments,
                comment,
            ],
        }
        
    }),
   
)