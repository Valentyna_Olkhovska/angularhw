import { createFeatureSelector, createSelector } from "@ngrx/store"
import { AppRootState } from "src/app/app.module";
import { CommentState } from "./taskComment.state"

export const selectCommentState = (state: AppRootState) => state.commentsStatate;

export const selectComments = createSelector(
    selectCommentState,
    (state: CommentState) => state.comments
);

