
import { createAction, props } from "@ngrx/store"
import { Comment } from "../../models/comment.model";



export const getAllComments = createAction(
    "[Task] Get all comments",
    props<{ comments: Comment[] }>()
);

export const addTaskComment = createAction(
    "[Task] Add task comment",
    props<{ comment: Comment }>()
);