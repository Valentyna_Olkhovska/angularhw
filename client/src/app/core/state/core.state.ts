import { DashboardsState } from './dashboard/dashboard.state'

export interface State {
    boards: DashboardsState;
}