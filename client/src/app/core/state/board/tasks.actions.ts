import { createAction, props } from "@ngrx/store"
import { Board } from "../../models/dashboard.model";
import { Task } from '../../models/task.model'

export const changeTaskStatus = createAction(
    "[Board] Change status",
    props<{taskId: string, nextStatus: string}>()
)

export const addNewTask = createAction(
    "[Board Created Form] Add new Task",
    props<{task: Task}>()
)

export const getAllTasks = createAction(
    "[Board] Get all tasks",
    props<{ board: Board, tasks: Task[] }>()
);

export const getTaskById = createAction(
    "[Task] Get task",
    props<{ taskId: string }>()
);

export const updateTask = createAction(
    "[Board Edit Form] Update task",
    props<{ id: string, name: string, description: string }>()
);

export const deleteTask = createAction(
    "[Board] Delete task",
    props<{task:Task}>()
);

export const appLoaded = createAction(
    "[App], App loaded"
);


export const fetchTasksSuccess = createAction(
    "[Board Api] Fetch Task Success",
    props<{ tasks: Task[] }>()
);

//ToDo

export const fetchTaskFailed = createAction(
    "[Board Api] Fetch Task Failed",
    props<{ error: any }>()
);