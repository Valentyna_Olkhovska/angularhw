import { createFeatureSelector, createSelector } from "@ngrx/store"
import { AppRootState } from "src/app/app.module";
import { BoardsState } from "./tasks.state"

export const selectTaskState = (state: AppRootState) => state.tasksState;

export const selectTasks = createSelector(
    selectTaskState,
    (state: BoardsState) => state.tasks
);

// export const selectTask = createSelector(
//     selectTaskState,
//     (state: BoardsState, taskId: string) =>  state.tasks.find(({_id}) => _id !== taskId ) 
// );
// export const selectTask = (taskId:string) => createSelector(selectTaskState, (state: BoardsState) => {
//     if (state.tasks) {
// console.log(1, taskId)
//       const a =  state.tasks.find(task => {
//          task._id === taskId;
//       });
//       console.log(state.tasks)
//       return a 
//     } else {
//       return {};
//     }
//   });
