import{Task} from '../../models/task.model'

export interface BoardsState {
    tasks: Task[]
}

export const initialState: BoardsState = {
    tasks: []
}