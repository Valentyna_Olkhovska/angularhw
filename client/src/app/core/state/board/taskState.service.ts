import { Injectable } from '@angular/core';
import { TaskService } from "src/app/api/task.service";
import { AppRootState } from "src/app/app.module";
import { Store } from "@ngrx/store"
import { Board } from '../../models/dashboard.model';
import { addNewTask, fetchTasksSuccess, changeTaskStatus, getTaskById,updateTask,deleteTask } from './tasks.actions';
import { Task } from '../../models/task.model';

@Injectable()
export class TaskStateService {
    constructor(
        private store: Store<AppRootState>,
        private TaskService: TaskService
    ) { }


    async fetchTasks(id: string): Promise<void> {

        this.TaskService.getTasks(id)
            .subscribe(({ tasks }) => {
                this.store.dispatch(fetchTasksSuccess({ tasks }));
            });

    }

    async addNewTask(newTask: Omit<Task, '_id' | 'createdDate'>, id: string): Promise<void> {

        this.TaskService.addTask(newTask, id)
            .subscribe(({ task }) => {
                this.store.dispatch(addNewTask({ 
                    task: {
                        ...task,
                        ...newTask,
                    }
                 }))
            })
    }

    async setTaskStatus(taskId: string, nextStatus: string): Promise<void> {
        this.TaskService.setTaskStatus(taskId, nextStatus).subscribe(() => {
            this.store.dispatch(changeTaskStatus({
                taskId,
                nextStatus
            }))
        });
    }

    async getTaskById(taskId: string): Promise<void> {
        this.TaskService.getTaskById(taskId).subscribe(() => {
            this.store.dispatch(getTaskById({
                taskId
            }))
        });
    }

        async updateTask(editedTask: Omit<Task, 'createdDate' | 'board' | 'status'>): Promise<void> {
            this.TaskService.editTask(editedTask)
                .subscribe(({ task }) => {
                    console.log(editedTask)
                    this.store.dispatch(updateTask({
                        id: editedTask._id, 
                        name: editedTask.name,
                        description: editedTask.description
                    }))

                })
        }

        async deleteTask(delTask:  Omit<Task,'createdDate' | 'board' | 'status' | 'name' | 'description'>): Promise<void> {
            this.TaskService.deleteTask(delTask)
                .subscribe(({ task }) => {
                    console.log(delTask)
                    this.store.dispatch(deleteTask({ 
                        task: {
                            ...task,
                            _id: delTask._id
                        }
                     }))
                })
        }


}