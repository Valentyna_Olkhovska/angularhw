import { createReducer, on } from "@ngrx/store"
import { initialState } from "./tasks.state"
import * as BoardAction from "./tasks.actions"

export const TaskReducer = createReducer(
    initialState,
    on(BoardAction.fetchTasksSuccess, (state, { tasks }) => {

        return {
            ...state,
            tasks
        }
    }),
    on(BoardAction.addNewTask, (state, { task }) => {
        return {
            ...state,
            tasks: [
                ...state.tasks,
                task,
            ],
        }
    }),
    on(BoardAction.changeTaskStatus, (state, { taskId, nextStatus }) => {
        return {
            ...state,
            tasks: state.tasks.map((task) => {
                if (task._id === taskId) {
                    return {
                        ...task,
                        status: nextStatus
                    };
                }

                return task;
            })
        }
    }),
    on(BoardAction.getTaskById, (state, { taskId }) => {
        const findTask = state.tasks.find(({ _id }) => _id !== taskId)

        return {
            ...state,
            findTask
        }
    }),


    on(BoardAction.updateTask, (state, { id, name,description }) => {
        const targetIndex = state.tasks.findIndex(({_id}) => id === _id);

        if (targetIndex === -1) {
            return state;
        }

        return {
            ...state,
            tasks: [
                ...state.tasks.slice(0, targetIndex),
                {
                    ...state.tasks[targetIndex],
                    name,
                    description
                },
                ...state.tasks.slice(targetIndex + 1)
            ],
        }
    }),




    on(BoardAction.deleteTask, (state, { task }) => {
        const filteredState = state.tasks.filter(({_id}) => task._id !== _id )

        return {
            ...state,
            tasks: [
                ...filteredState
            ],
        }
    }),

)
