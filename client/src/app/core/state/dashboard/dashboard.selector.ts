import { createFeatureSelector, createSelector } from "@ngrx/store"
import { AppRootState } from "src/app/app.module";
import { DashboardsState } from "./dashboard.state"

// export const selectDashboards = createFeatureSelector<DashboardsState>("boards");

export const selectBoardState = (state: AppRootState) => state.boardsState;

export const selectBoards = createSelector(
    selectBoardState,
    (state: DashboardsState) => state.boards
);
