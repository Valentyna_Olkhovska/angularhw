import { Injectable } from '@angular/core';
import { BoardService } from "src/app/api/boards.service";
import { AppRootState } from "src/app/app.module";
import { Store } from "@ngrx/store"
import { addNewBoard, deleteBoard, fetchBoardSuccess, updateBoard } from "./dashboard.actions";
import { Board } from '../../models/dashboard.model';

@Injectable()
export class DashboardStateService {
    constructor(
        private store: Store<AppRootState>,
        private BoardService: BoardService
    ) { }

    async fetchBoards(): Promise<void> {
        // All logic for call api service and put the response into the store
        this.BoardService.getBoards()
            .subscribe(({ boards }) => {
                this.store.dispatch(fetchBoardSuccess({ boards }));
            });
    }

    async addNewBoard(newBoard: Omit<Board, '_id' | 'createdDate' | 'tasks'>): Promise<void> {
        this.BoardService.addBoard(newBoard)
            .subscribe(({ board }) => {
                this.store.dispatch(addNewBoard({ board }))
            })
    }

    async updateBoard(editedBoard: Omit<Board, 'createdDate' | 'tasks'>): Promise<void> {
        this.BoardService.editBoard(editedBoard)
            .subscribe(({ board }) => {
                this.store.dispatch(updateBoard({
                    id: editedBoard._id, 
                    name: editedBoard.name
                }))
            })
    }

    async deleteBoard(delBoard:  Omit<Board,'createdDate' | 'tasks'>): Promise<void> {
        this.BoardService.deleteBoard(delBoard)
            .subscribe(({ board }) => {
                this.store.dispatch(deleteBoard({ 
                    board: {
                        ...board,
                        _id: delBoard._id
                    }
                 }))
            })
    }


}