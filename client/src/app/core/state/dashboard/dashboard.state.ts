import{Board} from './../../models/dashboard.model'

export interface DashboardsState {
    boards: Board[]
}

export const initialState: DashboardsState = {
    boards: []
}