import { createReducer, on } from "@ngrx/store"
import { initialState } from "./dashboard.state"
import * as DashBoardAction from "./dashboard.actions"

export const dashboardReducer = createReducer(
    initialState,
    on(DashBoardAction.fetchBoardSuccess, (state, { boards }) => {
        
        return {
            ...state,
            boards
        }
    }),
    on(DashBoardAction.addNewBoard, (state, { board }) => {
        return {
            ...state,
            boards: [
                ...state.boards,
                board,
            ],
        }
    }),
    on(DashBoardAction.updateBoard, (state, { id, name }) => {
        const targetIndex = state.boards.findIndex(({_id}) => id === _id);

        if (targetIndex === -1) {
            return state;
        }

        return {
            ...state,
            boards: [
                ...state.boards.slice(0, targetIndex),
                {
                    ...state.boards[targetIndex],
                    name,
                },
                ...state.boards.slice(targetIndex + 1)
            ],
        }
    }),
    on(DashBoardAction.deleteBoard, (state, { board }) => {
        const filteredState = state.boards.filter(({_id}) => board._id !== _id )

        return {
            ...state,
            boards: [
                ...filteredState
            ],
        }
    }),

)
