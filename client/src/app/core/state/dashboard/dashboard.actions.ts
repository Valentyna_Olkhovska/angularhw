import { createAction, props } from "@ngrx/store"
import { Board } from './../../models/dashboard.model'

export const addNewBoard = createAction(
    "[Dashboard Created Form] Add new Board",
    props<{board: Board}>()
)

export const getAllBoards = createAction(
    "[Home] Get all boards",
    props<{ boards: Board[] }>()
);

export const updateBoard = createAction(
    "[Dashboard Edit Form] Update board",
    props<{ id: string, name: string }>()
);

export const deleteBoard = createAction(
    "[Home] Delete board",
    props<{board:Board}>()
);

export const appLoaded = createAction(
    "[App], App loaded"
);

//ToDo

export const fetchBoardSuccess = createAction(
    "[Board Api] Fetch Board Success",
    props<{ boards: Board[] }>()
);

export const fetchBoardFailed = createAction(
    "[Board Api] Fetch Board Failed",
    props<{ error: any }>()
);