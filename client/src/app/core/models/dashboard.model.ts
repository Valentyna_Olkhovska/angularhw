export interface Board {
  _id: string,
  name: string,
  description?: string,
  createdDate: Date,
  tasks: [{
    name: string,
    taskId: string,
    status: string,
    _id: string
  } ]
}