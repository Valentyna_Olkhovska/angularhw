export interface Task {
    _id: string,
    name: string,
    description: string,
    createdDate: Date,
    board: string,
    status: string
  }