export interface Comment {
    _id: string,
    name: string,
    text: string,
    task: string
    createdDate: Date,
  }